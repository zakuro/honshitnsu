import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AuthGuard } from '@/app/guards/auth.guard'
import { AnonymousOnlyGuard } from '@/app/guards/anonymous-only.guard'

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    loadChildren: './pages/list/list.module#ListPageModule'
  },
  {
    path: 'categories/:categoryId',
    canActivate: [AuthGuard],
    loadChildren: './pages/list/list.module#ListPageModule'
  },
  {
    path: 'login',
    canActivate: [AnonymousOnlyGuard],
    loadChildren: './pages/login/login.module#LoginPageModule'
  },
  {
    path: 'config',
    canActivate: [AuthGuard],
    loadChildren: './pages/config/config.module#ConfigPageModule'
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
