import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs'
import { map, first, mergeMap, tap, filter } from 'rxjs/operators'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'

import { AuthService } from '@/app/services/auth.service'
import { Honshitsu } from '@/app/models/honshitsu'

@Injectable({
  providedIn: 'root'
})
export class HonshitsuService {
  honshitsusCollection: AngularFirestoreCollection<Honshitsu>
  list$: Observable<Honshitsu[]>
  constructor(
    private afs: AngularFirestore,
    private authService: AuthService,
  ) {
    this.list$ = authService.currentUser$.pipe(
      filter((currentUser) => !!currentUser),
      mergeMap((currentUser) => {
        this.honshitsusCollection = afs.collection<Honshitsu>(`users/${currentUser.uid}/honshitsus`, (ref) => ref.orderBy('value', 'desc'))
        return this.honshitsusCollection.valueChanges()
      }),
    )
  }

  insert(honshitsu: Honshitsu) {
    if (!honshitsu.id) {
      honshitsu.id = this.afs.createId()
    }
    return this.save(honshitsu)
  }

  save(honshitsu: Honshitsu) {
    return this.authService.currentUser$.pipe(
      first(),
      mergeMap((currentUser) => this.honshitsusCollection.doc(honshitsu.id).set(honshitsu)),
      map(() => honshitsu),
    )
  }

  remove(honshitsu: { id: string }) {
    return from(this.honshitsusCollection.doc(honshitsu.id).delete())
  }
}
