import { TestBed } from '@angular/core/testing';

import { HonshitsuService } from './honshitsu.service';

describe('HonshitsuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HonshitsuService = TestBed.get(HonshitsuService);
    expect(service).toBeTruthy();
  });
});
