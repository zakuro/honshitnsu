import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs'
import { map, first, mergeMap, tap, filter } from 'rxjs/operators'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'

import { AuthService } from '@/app/services/auth.service'
import { Category } from '@/app/models/category'

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  categoriesCollection: AngularFirestoreCollection<Category>
  list$: Observable<Category[]>
  constructor(
    private afs: AngularFirestore,
    private authService: AuthService,
  ) {
    this.list$ = authService.currentUser$.pipe(
      filter((currentUser) => !!currentUser),
      mergeMap((currentUser) => {
        this.categoriesCollection = afs.collection<Category>(`users/${currentUser.uid}/categories`)
        return this.categoriesCollection.valueChanges()
      }),
    )
  }

  insert(category: Category) {
    if (!category.id) {
      category.id = this.afs.createId()
    }
    return this.save(category)
  }

  save(category: Category) {
    return from(
      this.categoriesCollection.doc(category.id).set(category)
    ).pipe(
      map(() => category),
    )
  }

  remove(category: { id: string }) {
    return from(this.categoriesCollection.doc(category.id).delete())
  }
}
