import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs'
import { map, first, tap } from 'rxjs/operators'
import { AngularFireAuth } from '@angular/fire/auth'
import { User, auth } from 'firebase/app'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser$: Observable<User>
  loggedIn$: Observable<boolean>
  constructor(private afAuth: AngularFireAuth) {
    this.currentUser$ = this.afAuth.user
    this.loggedIn$ = this.currentUser$.pipe(map((currentUser) => !!currentUser))
  }

  loginAsAnonymous() {
    return from(this.afAuth.auth.signInAnonymously())
  }

  loginWithGoogle() {
    return from(this.afAuth.auth.signInWithRedirect(new auth.GoogleAuthProvider()))
  }

  logout() {
    return from(this.afAuth.auth.signOut())
  }
}
