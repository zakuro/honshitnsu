import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService } from '@/app/services/auth.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  loginAsAnonymous() {
    this.authService.loginAsAnonymous().subscribe(() => {
      this.router.navigate(['/'])
    })
  }

  loginWithGoogle() {
    this.authService.loginWithGoogle().subscribe(() => {
      this.router.navigate(['/'])
    })
  }
}
