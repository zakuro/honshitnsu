import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { ComponentFixture, TestBed, async } from '@angular/core/testing'
import { StoreModule } from '@ngrx/store'

import { reducers, metaReducers, Effects } from '@/app/store'
import { ListPage } from './list.page'

describe('ListPage', () => {
  let component: ListPage
  let fixture: ComponentFixture<ListPage>
  let listPage: HTMLElement

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPage ],
      imports: [
        StoreModule.forRoot(reducers, { metaReducers })
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents()
  }))

  beforeEach(async () => {
    fixture = await TestBed.createComponent(ListPage)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should have a list of 10 elements', () => {
    listPage = fixture.nativeElement
    const items = listPage.querySelectorAll('ion-item')
    expect(items.length).toEqual(10)
  })

})
