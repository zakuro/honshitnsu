import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { PopoverController, AlertController } from '@ionic/angular'
import { Observable, BehaviorSubject } from 'rxjs'
import { tap, first, map, mergeMap } from 'rxjs/operators'
import { Store, select } from '@ngrx/store'

import { State } from '@/app/store'
import {
  FetchAllHonshitsu,
  InsertHonshitsu,
  UpdateHonshitsu,
  RemoveHonshitsu,
  selectAllHonshitsu,
  selectHonshitsuList } from '@/app/store/honshitsu.store'
import { FetchAllCategories,
  InsertCategory,
  UpdateCategory,
  RemoveCategory,
  selectCategory,
  selectCategories } from '@/app/store/category.store'
import { Honshitsu } from '@/app/models/honshitsu'
import { Category } from '@/app/models/category'

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  list$: Observable<Honshitsu[]>
  categoryId$ = new BehaviorSubject<string>(null)
  category$: Observable<Category>
  categories$: Observable<Category[]>

  editing: boolean = false

  constructor(
    private alertCtrl: AlertController,
    private store: Store<State>,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.categories$ = this.store.pipe(select(selectCategories))
    this.category$ = this.categoryId$.pipe(mergeMap((id) => this.store.pipe(select(selectCategory, { id }))))
    this.list$ = this.categoryId$.pipe(
      mergeMap((categoryId) =>
        this.store.pipe(select(selectHonshitsuList, { categoryId }))
      ),
    )

    this.store.dispatch(new FetchAllHonshitsu())
    this.store.dispatch(new FetchAllCategories())
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.categoryId$.next(params.get('categoryId') || null)
    })
  }

  ngOnInit() {
  }

  createHonshitsu(honshitsu: Honshitsu) {
    this.store.dispatch(new InsertHonshitsu({ honshitsu }))
  }

  updateHonshitsu(honshitsu: Honshitsu) {
    this.store.dispatch(new UpdateHonshitsu({ honshitsu }))
  }

  removeHonshitsu(honshitsu: Honshitsu) {
    this.store.dispatch(new RemoveHonshitsu({ honshitsu }))
  }

  createCategory(category: Category) {
    this.store.dispatch(new InsertCategory({ category }))
  }

  updateCategory(category: Category) {
    this.store.dispatch(new UpdateCategory({ category }))
  }

  removeCategory(category: Category) {
    this.store.dispatch(new RemoveCategory({ category }))
    this.categoryId$.subscribe((categoryId) => {
      if (category.id === categoryId) {
        this.router.navigate(['/'])
      }
    })
  }
}
