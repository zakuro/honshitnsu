export interface Honshitsu {
  id: string
  title: string
  value: number
  categoryId?: string
}
