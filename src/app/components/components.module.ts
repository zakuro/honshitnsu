import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { IonicModule } from '@ionic/angular'

import { CategoryMenuComponent } from './category-menu/category-menu.component'
import { HonshitsuListComponent } from './honshitsu-list/honshitsu-list.component'

@NgModule({
  declarations: [
    CategoryMenuComponent,
    HonshitsuListComponent,
  ],
  imports: [CommonModule, IonicModule, RouterModule],
  exports: [
    CategoryMenuComponent,
    HonshitsuListComponent,
  ],
})
export class ComponentsModule {}
