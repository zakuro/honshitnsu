import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { AlertController } from '@ionic/angular'

import { Category } from '@/app/models/category'

@Component({
  selector: 'category-menu',
  templateUrl: './category-menu.component.html',
  styleUrls: ['./category-menu.component.scss']
})
export class CategoryMenuComponent implements OnInit {
  @Input() categories: Category[]

  @Output() update = new EventEmitter<Category>()
  @Output() create = new EventEmitter<Category>()
  @Output() remove = new EventEmitter<Category>()

  editing: boolean = false

  constructor(
    private alertCtrl: AlertController,
  ) {
  }

  ngOnInit() {
  }

  async newCategory() {
    this.editing = false
    const alert = await this.createCategoryAlert(
      { id: '', name: '', index: this.categories[this.categories.length - 1].index + 1 },
      (category) => { this.create.emit(category) }
    )
    alert.present()
  }

  async updateCategory(v: Category) {
    const alert = await this.createCategoryAlert(
      v,
      (category) => { this.update.emit(category) }
    )
    alert.present()
  }

  async createCategoryAlert(v: Category, handler: (v: Category) => void) {
    return await this.alertCtrl.create({
      header: 'カテゴリ',
      message: '<p id="category-alert-errors"></p>',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: '名前',
          value: v.name,
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'OK',
          handler: (form) => {
            function showError(msg: string) {
              const e = document.getElementById('category-alert-errors')
              e.innerHTML = msg
              return false
            }

            if (!form.name) {
              return showError('名前を入力してください')
            }

            v.name = form.name
            handler(v)
          }
        }
      ]
    })
  }

  reorder(e: any) {
    let before, after: number
    if (e.detail.to - e.detail.from > 0) {
      before = e.detail.to
      after = e.detail.to + 1
    } else {
      before = e.detail.to - 1
      after = e.detail.to
    }

    const last = this.categories.length -1
    if (before > last) { before = last }
    const beforeIndex = before < 0 ? this.categories[0].index - 1 : this.categories[before].index
    const afterIndex = after > last ? this.categories[last].index + 1 : this.categories[after].index

    const category = this.categories[e.detail.from]
    category.index = (beforeIndex + afterIndex) / 2.0
    console.log(before, after, beforeIndex, afterIndex)


    this.update.emit(category)
    e.detail.complete()
  }

  removeCategory(category: Category) {
    this.remove.emit(category)
  }
}
