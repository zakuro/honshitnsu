import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { AlertController } from '@ionic/angular'

import { Honshitsu } from '@/app/models/honshitsu'

@Component({
  selector: 'honshitsu-list',
  templateUrl: './honshitsu-list.component.html',
  styleUrls: ['./honshitsu-list.component.scss']
})
export class HonshitsuListComponent implements OnInit {
  @Input() list: Honshitsu[]
  @Input() categoryId: string
  @Output() create = new EventEmitter<Honshitsu>()
  @Output() update = new EventEmitter<Honshitsu>()
  @Output() remove = new EventEmitter<Honshitsu>()

  constructor(
    private alertCtrl: AlertController,
  ) {
  }

  ngOnInit() {
  }

  complete(honshitsu: Honshitsu) {
    this.remove.emit(honshitsu)
  }

  async newHonshitsu() {
    const alert = await this.createHonshitsuAlert(
      { id: '', title: '', value: 0, categoryId: this.categoryId },
      (honshitsu) => { this.create.emit(honshitsu) }
    )
    alert.present()
  }

  async updateHonshitsu(v: Honshitsu) {
    const alert = await this.createHonshitsuAlert(
      v,
      (honshitsu) => { this.update.emit(honshitsu) }
    )
    alert.present()
  }

  async createHonshitsuAlert(v: Honshitsu, handler: (v: Honshitsu) => void) {
    return await this.alertCtrl.create({
      header: '本質',
      message: '<p id="honshitsu-alert-errors"></p>',
      inputs: [
        {
          name: 'title',
          type: 'text',
          placeholder: '内容',
          value: v.title,
        },
        {
          name: 'value',
          type: 'number',
          placeholder: '本質ポイント',
          value: v.value.toString(),
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'OK',
          handler: (form) => {
            function showError(msg: string) {
              const e = document.getElementById('honshitsu-alert-errors')
              e.innerHTML = msg
              return false
            }

            if (!form.title) {
              return showError('内容を入力してください')
            }

            const value = Number(form.value)
            if (value === NaN) {
              return showError('ポイントは数値で入力してください')
            }

            v.title = form.title
            v.value = value
            handler(v)
          }
        }
      ]
    })
  }

  reorder(e: any) {
    const honshitsu = this.list[e.detail.from]
    const target = this.list[e.detail.to]
    // 一番下の下までドラッグしたときだけ undefined になる
    const targetValue = ((target === undefined) ? this.list[this.list.length - 1] : target).value
    // 下に行くなら次、上に行くなら前に入れるのが正解
    honshitsu.value  = targetValue + (e.detail.to - e.detail.from > 0 ? -1 : 1)

    this.update.emit(honshitsu)
    e.detail.complete(true)
  }

}
