import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HonshitsuListComponent } from './honshitsu-list.component';

describe('HonshitsuListComponent', () => {
  let component: HonshitsuListComponent;
  let fixture: ComponentFixture<HonshitsuListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HonshitsuListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HonshitsuListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
