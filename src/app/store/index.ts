import { ActionReducerMap } from '@ngrx/store'

import * as Honshitsu from './honshitsu.store'
import * as Category from './category.store'

export interface State {
  honshitsu: Honshitsu.State,
  category: Category.State,
}

export const reducers: ActionReducerMap<State> = {
  honshitsu: Honshitsu.reducer,
  category: Category.reducer,
}

export const Effects = [ Honshitsu.HonshitsuEffects, Category.CategoryEffects, ]
