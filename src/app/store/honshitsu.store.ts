import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'
import { Action, createFeatureSelector, createSelector } from '@ngrx/store'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity'

import { Honshitsu } from '@/app/models/honshitsu'
import { HonshitsuService } from '@/app/services/honshitsu.service'

export const honshitsuAdapter = createEntityAdapter<Honshitsu>()
export interface State extends EntityState<Honshitsu> {
  loading: boolean
}

export const initialState: State = {
  ...honshitsuAdapter.getInitialState(),
  loading: false,
}

export const selectFeature = createFeatureSelector<State>('honshitsu')
export const selectAllHonshitsu = createSelector(selectFeature, (state) =>
  // ids は string[] | number[] なので、any[] にしないと落ちる
  (state.ids as any[])
    .map((id) => state.entities[id])
    .sort((a, b) => b.value - a.value)
)
export const selectHonshitsuList = createSelector(selectAllHonshitsu, (list, props) =>
  list.filter((v) => !props.categoryId || v.categoryId === props.categoryId)
)

export enum HonshitsuActionTypes {
  FetchAll = '[Honshitsu] Fetch All',
  LoadAll = '[Honshitsu] Load All',
  Insert = '[Honshitsu] Insert',
  Update = '[Honshitsu] Update',
  Remove = '[Honshitsu] Remove',
  SelectCategory = '[Honshitsu] Select Category'
}

export class FetchAllHonshitsu implements Action {
  readonly type= HonshitsuActionTypes.FetchAll
}

export class LoadAllHonshitsu implements Action {
  readonly type= HonshitsuActionTypes.LoadAll
  constructor(public payload: { honshitsus: Honshitsu[] }) {}
}

export class InsertHonshitsu implements Action {
  readonly type= HonshitsuActionTypes.Insert
  constructor(public payload: { honshitsu: Honshitsu }) {}
}

export class UpdateHonshitsu implements Action {
  readonly type= HonshitsuActionTypes.Update
  constructor(public payload: { honshitsu: Honshitsu }) {}
}

export class RemoveHonshitsu implements Action {
  readonly type= HonshitsuActionTypes.Remove
  constructor(public payload: { honshitsu: { id: string } }) {}
}

export type HonshitsuActions = FetchAllHonshitsu
  | LoadAllHonshitsu
  | InsertHonshitsu
  | UpdateHonshitsu
  | RemoveHonshitsu

export function reducer(state: State = initialState, action: HonshitsuActions) {
  switch(action.type) {
    case HonshitsuActionTypes.FetchAll: {
      return { ...state, loading: true }
    }
    case HonshitsuActionTypes.LoadAll: {
      return { ...honshitsuAdapter.addAll(action.payload.honshitsus, state), loading: false }
    }
    case HonshitsuActionTypes.Insert: {
      return { ...state, loading: true }
    }
    case HonshitsuActionTypes.Update: {
      const { honshitsu } = action.payload
      return honshitsuAdapter.updateOne({ id: honshitsu.id, changes: honshitsu } , state)
    }
    case HonshitsuActionTypes.Remove: {
      return honshitsuAdapter.removeOne(action.payload.honshitsu.id, state)
    }
  }
  return state
}

@Injectable()
export class HonshitsuEffects {
  constructor(
    private actions$: Actions,
    private honshitsuService: HonshitsuService,
  ) {}

  @Effect()
  FetchAll$ = this.actions$.pipe(
    ofType<FetchAllHonshitsu>(HonshitsuActionTypes.FetchAll),
    switchMap((action) =>
      this.honshitsuService.list$.pipe(
        map((honshitsus) => new LoadAllHonshitsu({ honshitsus }))
      )
    )
  )

  @Effect()
  Insert$ = this.actions$.pipe(
    ofType<InsertHonshitsu>(HonshitsuActionTypes.Insert),
    switchMap((action) => this.honshitsuService.insert(action.payload.honshitsu)),
    map(() => new FetchAllHonshitsu()),
  )

  @Effect({ dispatch: false })
  Update$ = this.actions$.pipe(
    ofType<UpdateHonshitsu>(HonshitsuActionTypes.Update),
    switchMap((action) => this.honshitsuService.save(action.payload.honshitsu)),
  )

  @Effect({ dispatch: false })
  Remove$ = this.actions$.pipe(
    ofType<RemoveHonshitsu>(HonshitsuActionTypes.Remove),
    switchMap((action) => this.honshitsuService.remove(action.payload.honshitsu)),
  )
}
