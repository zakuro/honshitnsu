import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'
import { Action, createFeatureSelector, createSelector } from '@ngrx/store'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity'

import { Category } from '@/app/models/category'
import { CategoryService } from '@/app/services/category.service'

export const categoryAdapter = createEntityAdapter<Category>()
export interface State extends EntityState<Category> {
  loading: boolean
}

export const initialState: State = {
  ...categoryAdapter.getInitialState(),
  loading: false,
}

export const selectFeature = createFeatureSelector<State>('category')
export const selectCategories = createSelector(selectFeature,
  (state) => (state.ids as any[]).map((id) => state.entities[id]).sort((a, b) => a.index - b.index)
)
export const selectCategory = createSelector(selectFeature, (state, props) => props.id ? state.entities[props.id] : null)

export enum CategoryActionTypes {
  FetchAll = '[Category] Fetch All',
  LoadAll = '[Category] Load All',
  Insert = '[Category] Insert',
  Update = '[Category] Update',
  Remove = '[Category] Remove',
}

export class FetchAllCategories implements Action {
  readonly type= CategoryActionTypes.FetchAll
}

export class LoadAllCategories implements Action {
  readonly type= CategoryActionTypes.LoadAll
  constructor(public payload: { categories: Category[] }) {}
}

export class InsertCategory implements Action {
  readonly type= CategoryActionTypes.Insert
  constructor(public payload: { category: Category }) {}
}

export class UpdateCategory implements Action {
  readonly type= CategoryActionTypes.Update
  constructor(public payload: { category: Category }) {}
}

export class RemoveCategory implements Action {
  readonly type= CategoryActionTypes.Remove
  constructor(public payload: { category: { id: string } }) {}
}

export type CategoryActions = FetchAllCategories | LoadAllCategories | InsertCategory | UpdateCategory | RemoveCategory

export function reducer(state: State = initialState, action: CategoryActions) {
  switch(action.type) {
    case CategoryActionTypes.FetchAll: {
      return { ...state, loading: true }
    }
    case CategoryActionTypes.LoadAll: {
      return { ...categoryAdapter.addAll(action.payload.categories, state), loading: false }
    }
    case CategoryActionTypes.Insert: {
      return { ...state, loading: true }
    }
    case CategoryActionTypes.Update: {
      const { category } = action.payload
      return categoryAdapter.updateOne({ id: category.id, changes: category } , state)
    }
    case CategoryActionTypes.Remove: {
      return categoryAdapter.removeOne(action.payload.category.id, state)
    }
  }
  return state
}

@Injectable()
export class CategoryEffects {
  constructor(
    private actions$: Actions,
    private categoryService: CategoryService,
  ) {}

  @Effect()
  FetchAll$ = this.actions$.pipe(
    ofType<FetchAllCategories>(CategoryActionTypes.FetchAll),
    switchMap((action) =>
      this.categoryService.list$.pipe(
        map((categories) => new LoadAllCategories({ categories }))
      )
    )
  )

  @Effect()
  Insert$ = this.actions$.pipe(
    ofType<InsertCategory>(CategoryActionTypes.Insert),
    switchMap((action) => this.categoryService.insert(action.payload.category)),
    map(() => new FetchAllCategories()),
  )

  @Effect({ dispatch: false })
  Update$ = this.actions$.pipe(
    ofType<UpdateCategory>(CategoryActionTypes.Update),
    switchMap((action) => this.categoryService.save(action.payload.category)),
  )

  @Effect({ dispatch: false })
  Remove$ = this.actions$.pipe(
    ofType<RemoveCategory>(CategoryActionTypes.Remove),
    switchMap((action) => this.categoryService.remove(action.payload.category)),
  )
}
